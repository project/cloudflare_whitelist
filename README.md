# Cloudflare Whitelist

Automatically add logged in users to the Cloudflare firewall whitelist. This will allow them to bypass any extra filtering put in place by Cloudflare.

The module provides a UI to see all the rules added by the module and the ability to delete them. The admin url is `/admin/config/development/cloudflare-whitelist`

This module works with `v4` of the [Cloudflare API](https://api.cloudflare.com/#user-level-firewall-access-rule-properties).

Special thanks to the [Mises Institute](http://mises.org) for sharing.

## Setup

### Cloudflare API.

Get your Cloudflare API key from the My Profile page in Cloudflare.

You will want the `Global API Key`.

### Drupal

You need to add the following config to your `settings.php` file.

The email is the email address you use to login in to Cloudflare.

```php
  $conf['cloudflare_whitelist_auth_email'] = 'example@example.com';
  $conf['cloudflare_whitelist_auth_key'] = 'XXXXXXXXX9999999';
```

Alternatively you can also set those variables with Drush

```bash
drush vset cloudflare_whitelist_auth_email 'example@example.com'
drush vset cloudflare_whitelist_auth_key 'XXXXXXXXX9999999'
``` 
