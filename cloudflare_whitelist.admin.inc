<?php

/**
 * @file
 * Admin UI forms.
 */

/**
 * Implements hook_form().
 */
function cloudflare_whitelist_settings_form($form, &$form_state) {

  if (!cloudflare_whitelist_check_auth()) {
    return;
  }

  $params = [
    'mode' => 'whitelist',
    'notes' => cloudflare_whitelist_api_rule_key(),
    'per_page' => 500,
  ];
  $response = cloudflare_whitelist_api_list_rules($params);

  if (empty($response->result)) {
    $form['empty'] = array(
      '#markup' => '<p>' . t('There are no users whitelisted at this time.') . '</p>',
    );
    return $form;
  }

  $headers = [
    t('User'),
    t('IP'),
    t('Created'),
    t('Actions'),
  ];

  $rows = [];

  foreach ($response->result as $rule) {
    $username = cloudflare_whitelist_parse_key($rule->notes)[2];
    $rule_id = $rule->id;
    $button_name = 'delete_' . $rule_id . '_' . $username;
    $form[$button_name] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#name' => $button_name,
    ];

    $created_date = explode('T', $rule->created_on)[0];

    $row = [
      $username,
      $rule->configuration->value,
      $created_date,
      drupal_render($form[$button_name]),
    ];

    $rows[] = $row;
  }

  $form['whitelist'] = [
    '#theme' => 'table',
    '#header' => $headers,
    '#tree' => TRUE,
    '#rows' => $rows,
  ];

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function cloudflare_whitelist_settings_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] = 'Delete') {
    $rule_id = explode('_', $form_state['clicked_button']['#name'])[1];
    $username = explode('_', $form_state['clicked_button']['#name'])[2];
    cloudflare_whitelist_api_delete_rule($rule_id, $username);
  }
}
